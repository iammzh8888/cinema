const DOMAIN = "http://localhost:3002"

var currId = 0; // id of the currently selected item, 0 if none
$('#exampleModal').on('hidden.bs.modal', function() {
    currId = 0;

    document.getElementById("movieForm").reset();
    $("#saveOrAdd").html("Add movie");
});

/******************************** movie **********************************/
$(document).ready(function() {
    displayMovies();
    $("#saveOrAdd").html("Add movie");

});

//display al list of movies
function displayMovies() {
    $.ajax({
        method: "GET",
        url: `${DOMAIN}/movies` + ((document.getElementById("search").value == "") ? "" : "?name=" + document.getElementById("search").value)
    }).done((resp) => {
        let result = '<thead><tr class="table-secondary"><th>#</th><th>name</th><th>Start Time</th><th>End Time</th><th>Price</th><th>is Full?</th><th>Edit</th><th>Delete</th></tr></thead>';
        resp.forEach(movie => {
            result += '<tr>';
            result += '<td>' + `${movie.id}` + '</td>';
            result += '<td>' + `${movie.name}` + '</td>';
            result += '<td>' + `${movie.startTime}` + '</td>';
            result += '<td>' + `${movie.endTime}` + '</td>';
            result += '<td>' + `${movie.price}` + '</td>';
            result += '<td>' + `${movie.isFull ? 'Yes' : 'No'}` + '</td>';
            result += '<td><button class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick = selectItem(' + `${movie.id}` + ') ><i class="fas fas fa-pen"></i></button></td>';
            result += '<td><button class="btn btn-danger" onclick = deleteMovie(' + `${movie.id}` + ') ><i class="fas fa-trash-alt"></i></button></td>';
            result += '</tr>' + "\n";
        });
        $("#listTable").html(result);
    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });
}

//Add a movie
function addMovie() {
    const nameVal = $("input[name=name]").val();
    const startTimeVal = $("input[name=startTime]").val();
    const endTimeVal = $("input[name=endTime]").val();
    const price = $("input[name=price]").val();
    const isFull = $("input[name=isFull]:checked").val();

    var movieObj = {
        name: nameVal,
        startTime: startTimeVal,
        endTime: endTimeVal,
        price: price,
        isFull: isFull
    };

    $.ajax({
        url: `${DOMAIN}/movies` + (currId == 0 ? "" : "/" + currId),
        type: currId == 0 ? 'POST' : 'PUT',
        dataType: "json",
        data: movieObj
    }).done((resp) => {
        alert("Record " + (currId == 0 ? "added" : "updated") + " successfully");
        currId = 0;
        $('#exampleModal').modal('hide')
        location.reload();

    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });
}

//Delete a movie
function deleteMovie(id) {
    if (id == 0) return;
    if (!confirm("Are you sure you want to delete this item?")) return;
    $.ajax({
        url: `${DOMAIN}/movies/` + id,
        type: 'DELETE',
        dataType: "json"
    }).done((resp) => {
        alert("Deleted successfully");
        location.reload();
    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });
}

//select a movie for edit
function selectItem(id) {
    $.ajax({
        url: `${DOMAIN}/movies/` + id,
        method: "GET"
    }).done((resp) => {
        resp.forEach(movie => {
            currId = id;
            document.getElementById("name").value = movie.name;
            document.getElementById("startTime").value = movie.starttime;
            document.getElementById("endTime").value = movie.endtime;
            document.getElementById("price").value = movie.price;
            if (movie.isfull == 1) {
                $("input[name='isFull'][value=1]").attr("checked", true);
            } else if (movie.isfull == 0) {
                $("input[name='isFull'][value=0]").attr("checked", true);
            }
            $("#saveOrAdd").html("Update movie");
            $("#exampleModalLabel").html("Update movie");

        });

    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });

}

//select a movie for booking
function bookItem(id) {
    $.ajax({
        url: `${DOMAIN}/movies/` + id,
        method: "GET"
    }).done((resp) => {
        resp.forEach(movie => {
            currId = id;
            document.getElementById("bookName").innerHTML = movie.name;
            document.getElementById("bookStartTime").innerHTML = movie.starttime;
            document.getElementById("bookEndTime").innerHTML = movie.endtime;
            document.getElementById("bookPrice").innerHTML = movie.price;
            if (movie.isfull) {
                document.getElementById("bookIsNotFull").style.display = "none";
                document.getElementById("bookIsFull").style.display = "block";
            } else {
                document.getElementById("bookIsFull").style.display = "none";
                document.getElementById("bookIsNotFull").style.display = "block";
            }
        });

    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });

}

//Book a movie
function bookMovie() {
    const orderDateVal = Date.now();
    const audienceIdVal = global.audienceId;
    const movieIdVal = currId;

    var orderObj = {
        name: nameVal,
        audienceid: startTimeVal,
        endTime: endTimeVal,
        price: price,
        isFull: isFull
    };

    $.ajax({
        url: `${DOMAIN}/movies` + (currId == 0 ? "" : "/" + currId),
        type: currId == 0 ? 'POST' : 'PUT',
        dataType: "json",
        data: movieObj
    }).done((resp) => {
        alert("Record " + (currId == 0 ? "added" : "updated") + " successfully");
        currId = 0;
        $('#exampleModal').modal('hide')
        location.reload();

    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });
}