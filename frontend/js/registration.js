const DOMAIN = "http://localhost:3002"

/******************************** Book a ticket **********************************/
//populate dropdown list - Get all movies
$(document).ready(function() {
    $.ajax({
        method: "GET",
        url: `${DOMAIN}/movies`
    }).done((resp) => {
        let dropdown = $('#movie-dropdown');
        dropdown.empty();
        dropdown.append('<option selected="true" value=0 disabled>Choose movies</option>');
        resp.forEach(movie => {
            dropdown.prop('selectedIndex', 0);
            dropdown.append($('<option></option>').attr('value', `${movie.id}`).text(`${movie.name}`));
        });
    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });
});

//Add an order
function addRegistration() {
    const movieIdVal = parseInt(document.getElementById("movie-dropdown").value);
    const nameVal = $("input[name=name]").val();
    const passwordVal = $("input[name=password]").val();
    const emailVal = $("input[name=email]").val();
    const phoneVal = $("input[name=phone]").val();
    let audienceObj = {
        name: nameVal,
        password: passwordVal,
        email: emailVal,
        phone: phoneVal
    };
    var audienceIdVal = 0;

    $.ajax({
        url: `${DOMAIN}/registration`,
        type: 'POST',
        dataType: "json",
        async: false,
        data: audienceObj
    }).done((resp) => {
        audienceIdVal = resp.insertId;
        alert("Audience added successfully");
        location.reload();
    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });

    let orderObj = {
        audienceid: audienceIdVal,
        movieid: movieIdVal
    };
    $.ajax({
        url: `${DOMAIN}/orders`,
        type: 'POST',
        dataType: "json",
        data: orderObj
    }).done((resp) => {
        alert("Order added successfully");
        // location.href = `${DOMAIN}/registration`;
        // location.reload;
    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });
}