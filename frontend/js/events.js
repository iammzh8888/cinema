const DOMAIN = "http://localhost:3002"
    //const loginAudi = require('example');
    /******************************** Reports **********************************/
$(document).ready(function() {
    $.ajax({
        method: "GET",
        url: `${DOMAIN}/report`
    }).done((resp) => {
        let container = document.getElementById("reportlistTable")

        resp.forEach(report => {
            let input = document.createElement("input");
            input.type = "hidden";
            input.className = "name";
            input.value = report.name;
            container.appendChild(input);
            let input2 = document.createElement("input");
            input2.type = "hidden";
            input2.className = "cost";
            input2.value = report.cost;
            container.appendChild(input2);

        });
    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });

    $.ajax({
        method: "GET",
        url: `${DOMAIN}/report/piechart`
    }).done((resp) => {
        let container = document.getElementById("reportlistTable")

        resp.forEach(report => {
            let input = document.createElement("input");
            input.type = "hidden";
            input.className = "attendance";
            input.value = report.attendance;
            container.appendChild(input);
            let input2 = document.createElement("input");
            input2.type = "hidden";
            input2.className = "count";
            input2.value = report.count;
            container.appendChild(input2);

        });
    }).fail((xhrObj, textStatus) => {
        // API sends an error . Response code in 400s or 500s
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
    });
});

//Display Charts
$(window).on('load', function() {
    let barChartData = [];
    var element = document.getElementsByClassName('cost');
    for (let i = 0, l = element.length; i < l; i++) {
        barChartData[i] = element[i].value;
    }

    let barChartLabel = [];
    var element = document.getElementsByClassName('name');
    for (let i = 0, l = element.length; i < l; i++) {
        barChartLabel[i] = element[i].value;
    }
    let ctx1 = document.getElementById("barChart");
    let myBarChart = new Chart(ctx1, {
        type: 'bar',
        data: {
            labels: barChartLabel,
            datasets: [{
                data: barChartData,
                backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"]

            }]
        },
        options: {
            legend: { display: false },
            title: {
                display: false,
                text: ''
            }
        }
    });
    let pieChartLabel = [];
    var element = document.getElementsByClassName('attendance');
    for (let i = 0, l = element.length; i < l; i++) {
        pieChartLabel[i] = element[i].value;
    }
    let pieChartData = [];
    var element = document.getElementsByClassName('count');
    for (let i = 0, l = element.length; i < l; i++) {
        pieChartData[i] = element[i].value;
    }

    let ctx2 = document.getElementById("pieChart");

    let myPieChart = new Chart(ctx2, {
        type: 'pie',
        data: {
            labels: pieChartLabel,
            datasets: [{
                data: pieChartData,
                backgroundColor: ["#8e5ea2", "#c45850", "#3e95cd", "#e8c3b9", "#3cba9f"]
            }]
        }
    });
});