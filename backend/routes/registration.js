const express = require('express');
const Joi = require('joi')

const router = express.Router();

const sql = require('../db')
const validator = require('express-joi-validation').createValidator({ passError: true })

//Validation Schemas
const audienceSchema = Joi.object({
    name: Joi.string().max(15).required(),
    password: Joi.string().min(3).required(),
    email: Joi.string().email().required(),
    phone: Joi.string().max(15).required()
});

/* POST - Add an Audience */
router.post('/', validator.body(audienceSchema), function(req, res, next) {
    sql.query('INSERT INTO audiences SET ?', req.body, (error, result) => {
        if (error) {
            next(error);
        }
        // global.AudienceId = result.insertId;
        // global.AudienceName = req.body.name;
        res.json(result);
    });
});

module.exports = router;