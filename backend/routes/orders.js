const express = require('express');
const Joi = require('joi')

const router = express.Router();

const sql = require('../db')
const validator = require('express-joi-validation').createValidator({ passError: true })

/* POST - Add an order */
router.post('/', function(req, res, next) {
    sql.query('INSERT INTO orders SET ?', req.body, (error, result) => {
        if (error) {
            next(error);
        }
        // global.AudienceId = result.insertId;
        // global.AudienceName = req.body.name;
        res.json(result);
    });
});

module.exports = router;