const express = require('express');
const Joi = require('joi')

const router = express.Router();

const sql = require('../db')
const validator = require('express-joi-validation').createValidator({ passError: true })

//Validation Schemas
const movieSchema = Joi.object({
    name: Joi.string().max(30).required(),
    startTime: Joi.date().greater('now').required(),
    endTime: Joi.date().greater(Joi.ref('startTime')).required(),
    price: Joi.number().min(0).required(),
    isFull: Joi.required()
});

const movieParam = Joi.object({
    name: Joi.string().max(20).optional()
});


/* GET - Display all movies. */
router.get('/', validator.query(movieParam), function(req, res, next) {
    let sqlString = "SELECT id, name , startTime , endTime , price , isFull FROM movies";
    if (req.query && req.query.name) {
        sqlString += ` WHERE name LIKE '%${req.query.name}%'`
    }
    sql.query(sqlString, function(error, results, fields) {
        if (error) {
            next(error);
        }
        res.json(results);
    });
});

/* POST - Add a movie */
router.post('/', validator.body(movieSchema), function(req, res, next) {
    const data = req.body;
    sql.query('INSERT INTO movies SET ?', data, function(error, results, fields) {
        if (error) {
            next(error);
        }
        res.json(results);
    });
});

/* Delete - Delete a movie */
router.delete('/:id', function(req, res, next) {
    console.log(req.params);
    sql.query('DELETE FROM movies WHERE id=?', [req.params.id], function(error, results, fields) {
        if (error) {
            res.status(404).send({ message: "Enrollment not found" });
        }
        res.json(results);
    });
});

// GET - get one movie by id
router.get('/:id', function(req, res, next) {
    console.log(req.params);
    sql.query("SELECT * FROM movies WHERE id=?", [req.params.id], function(error, results, fields) {
        if (error) {
            res.status(404).send({ message: "Movie not found" });
        }
        res.json(results);
    });
});

//PUT API to update one movie by id
router.put('/:id', validator.body(movieSchema), function(req, res, next) {
    console.log(req.params);
    sql.query('UPDATE movies SET name=?, starttime=?, endtime=?, price=?, isfull=? where id=?', [req.body.name, req.body.startTime, req.body.endTime, req.body.price, req.body.isFull, req.params.id], function(error, results, fields) {
        if (error) {
            res.status(404).send({ message: "Movie not found" });
        }
        res.json(results);
    });
});

module.exports = router;