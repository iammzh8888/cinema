'use strict';

const mysql = require('mysql');

//local mysql db connections
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'iammzh8888',
    password: 'root',
    database: 'cinemadb'
});

connection.connect(function(err) {
    if (err) throw err;
    console.log("DB connection successful");
});

module.exports = connection;