CREATE DATABASE cinemadb;

use cinemadb;

CREATE TABLE IF NOT EXISTS `movies` (
	`id` int(11) NOT NULL,
    `name` varchar(30) NOT NULL,
	`starttime` datetime NOT NULL,
    `endtime` datetime NOT NULL,
	`price` decimal NOT NULL,
	`isfull` boolean NOT NULL
  );

ALTER TABLE `movies` ADD PRIMARY KEY (`id`);
ALTER TABLE `movies` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `movies` (`name`, `starttime` , `endtime` , `price` , `isfull`) VALUES
('Ice Age', '2021-05-09 10:00:00', '2021-05-09 12:30:00' , 23 , false ),
('Zootopia', '2021-05-09 11:10:00', '2021-05-09 13:00:00' , 25 , false ),
('Brave Heart', '2021-05-09 14:00:00', '2021-05-09 16:10:00' , 15 , false ),
('Without Remorse', '2021-05-09 15:20:00', '2021-05-09 16:00:00' , 30 , false ),
('Things Heard and Seen', '2021-05-09 16:00:00', '2021-05-09 18:10:00' , 28 , false );

CREATE TABLE IF NOT EXISTS `audiences` (
	`id` int(11) NOT NULL,
    `name` varchar(30) NOT NULL,
	`registerdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `password` varchar(20) NOT NULL,
    `email` varchar(30) NOT NULL,
    `phone` varchar(15) NOT NULL
  );

ALTER TABLE `audiences` ADD PRIMARY KEY (`id`);
ALTER TABLE `audiences` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `audiences` (`name`, `registerdate` , `password` , `email` , `phone` ) VALUES
('Elena Soy', '2021-03-25 15:02:00', '123456', 'elena@gmail.com', '5142345456'),
('Elsa Morgan', '2018-12-10 10:11:00', '123456', 'elsa@hotmail.com','5143253445');

CREATE TABLE IF NOT EXISTS `orders` (
	`id` int(11) NOT NULL,
	`orderdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `audienceid` int(11) NOT NULL,
    `movieid` int(11) NOT NULL
  );

ALTER TABLE `orders` ADD PRIMARY KEY (`id`);
ALTER TABLE `orders` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `orders` ADD constraint fk_audience_order foreign key (audienceid) references audiences (id);
ALTER TABLE `orders` ADD constraint fk_movie_order foreign key (movieid) references movies (id);

INSERT INTO `orders` (`orderdate`, `audienceid` , `movieid` ) VALUES
('2021-05-04 15:20:00', 1, 1),
('2021-05-05 12:30:00', 2, 3);